#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define LOCATIONS 1
#define BARCODES 0

/* A program to validate that the data given through stdin is proper data that can be analysed by the 
   other validation programs. Depending on the program arguments, it will validate either barcode strings
   or location data, and filter the text with corresponding criteria. */

int checkLength(char[], int);
int checkNonDigits(char[], int);

int checkNonDigits(char string[], int type) {
	int i = 0;
	if(type == LOCATIONS) {
		for(i = 0; i < 4; ++i) {
			if(!isdigit(string[i])) {
				fprintf(stderr, "Error: %s has non-numerical characters\n", string);
				return 1;
			}
		}

		return 0;

	} else if(type == BARCODES) {
		for(i = 0; i < 12; ++i) {
			if(!isdigit(string[i])) {
				fprintf(stderr, "Error: %s has non-numerical characters\n", string);
				return 1;
			}
		}

		return 0;
	}

    return 0;
}

int checkLength(char string[], int type) {
	if(type == LOCATIONS) {
		if(strlen(string) != 4){
			fprintf(stderr, "Error: %s incorrect length for location data\n", string);
			return 1;
		} else 
			return 0;

	} else if (type == BARCODES) {
		if(strlen(string) != 12) {
			fprintf(stderr, "Error: %s incorrect length for barcode data\n", string);
			return 1;
		} else
			return 0;
	}

    return 0;
}
int main(int argc, char *argv[]) {
	int i = 0;
	FILE *fip;
	FILE *fop = fopen("validlocations.txt", "w");
	/* parse program arguments */
	for(i = 1; i < argc; ++i) {
		if(strcmp(argv[i], "--locations") == 0) {
			fip = fopen(argv[i+1], "r");
		}
	}

	/* If neither arguments are given, end the program and give warning. */
	if(argc != 3) {
		fprintf(stderr, "Please provide program arguments.");
		return 0;
	}

	if(fip == NULL) {
		fprintf(stderr, "File not found.");
		return 0;
	}

	/* process locations first */

	char buff_loc[256];
	char buff_barcode[256];

	while(fgets(buff_loc, sizeof(buff_loc), fip) != NULL) {
		int length = strlen(buff_loc);
		buff_loc[length - 1] = '\0';
		int lengthCheck = checkLength(buff_loc, LOCATIONS);
		if (lengthCheck == 1)
			continue;
		int digitCheck = checkNonDigits(buff_loc, LOCATIONS);
		if (digitCheck == 1)
			continue;

		fprintf(fop, "%s\n", buff_loc);
	}

	fclose(fop);
	fclose(fip);

		/* now the barcodes */

		while(fgets(buff_barcode, sizeof(buff_barcode), stdin) != NULL) {
			int length = strlen(buff_barcode);
			buff_barcode[length - 1] = '\0';
			int lengthCheck = checkLength(buff_barcode, BARCODES);
			if (lengthCheck == 1)
				continue;
			int digitCheck = checkNonDigits(buff_barcode, BARCODES);
			if (digitCheck == 1)
				continue;

			fprintf(stdout, "%s\n", buff_barcode);
		}



	return 0;

}
