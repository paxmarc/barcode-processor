CC = gcc
CFLAGS = -std=gnu99 -W -Wall

.PHONY: all clean

all: check_location checksum validate_input

check_location: check_location.c
	$(CC) $(CFLAGS) -o check_location check_location.c

checksum: checksum.c
	$(CC) $(CFLAGS) -o checksum checksum.c

validate_input: validate_input.c
	$(CC) $(CFLAGS) -o validate_input validate_input.c

clean:
	rm ./check_location 
	rm ./checksum  
	rm ./validate_input
