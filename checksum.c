#include <stdio.h>

/* program to validate the checksum of a barcode, input through
 * stdin. the checksum is the last digit, which needs to be the
 * last digit of the addition of the others. the program will
 * print the barcode to stdout if it is valid. We can assume valid input. */

void checkCurrentBarcode(int[], int);

void checkCurrentBarcode(int barcode[], int linelength) {
	int index, checksum;
	/* initialise needed values */
	checksum = 0;
	if(linelength != 12) {
		return;
	}

	for(index = 0; index < 11; index++) {
		checksum = checksum + barcode[index];
	}

	/*check the last digit. as the sum of the first 11 can't go over 99, checksum % 10 will work */
	checksum = checksum % 10;
	/* if it is valid, print the barcode, otherwise do nothing */
	if(checksum == barcode[11]) {
		for(index = 0; index < 12; index++) {
			putchar(barcode[index] + '0');
		}
		putchar('\n');
		return;
	} else {
		fprintf(stderr, "Error: ");
		for(index = 0; index < 12; index++)
			fprintf(stderr, "%d", barcode[index]);
		fprintf(stderr, " has incorrect checksum\n");
		return;
	} 
	;
}

int main(void) {
	int c, index, linelength;
	int barcode[12];
	index = linelength = 0;

	/* read in barcodes, one character at a time */
	while((c = getchar())  != EOF) {
		/* check for new line, if so, reset the counter */
		if( c == '\n') {
			checkCurrentBarcode(barcode, linelength);
			index = 0;
			linelength = 0;
			continue;
		}
		/* otherwise, feed the values in, converting the char to an int */
		barcode[index] = c - '0';
		index++;
		linelength++;
	}

	return 0;

}
