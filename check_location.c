#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* Program to check the validity of a given barcode by checking the 4-digit
 * location number, which is the first 4 digits of the barcode. Using the
 * text file given in the --locations argument, it will check the first 4 digits of 
 * the barcode. 
 *
 * - If valid, it will print the barcode to stdout.
 * - Otherwise, it will not print.
 *
 * - Will take the barcodes from stdin. */

void checklocation(FILE*, char[]);

void checklocation(FILE *fp, char string[]) {

	/* create array to hold location string in current barcode */
	char loc[5];

	/* extract the location string */
	strncpy(loc, string, 5);
	loc[4] = '\0';


	char buff[256];

	/* create pointer to beginning of file to return to for every new barcode */
	fpos_t position;
	fgetpos(fp, &position);

	/* go through the locations file, and see if the one in the barcode is present */
	while(fgets(buff, sizeof(buff), fp) != NULL) {
		buff[4] = '\0';
		if(strcmp(loc, buff) == 0) {
			/* if it is, print it out */
			fprintf(stdout,"%s\n", string);
			fsetpos(fp, &position);
			return;
		} 
	}
	fprintf(stderr, "Error: barcode %s contains invalid location %s\n", string, loc);

	/* move the position pointer back to the beginning of the file */
	fsetpos(fp, &position);
	return;
}

int main(int argc, char *argv[]) {

	int i;
	FILE *fp;
	char buff[256];
	

	/* look for the --locations argument, and the filename in the argument
	 * following it */
	for(i = 1; i < argc; i++) {
		if(strcmp(argv[i], "--locations") == 0) {
			fp = fopen(argv[i+1], "r");
		}
	}

	/* if file not found, print that to stderr. */
	if(fp == NULL) {
		fprintf(stderr, "Error: Locations file not found.\n");
		return 1;
	}

	/* get the next line from stdin, cut off the \n, and pass it into the 
	 * checklocation function */
	while(fgets(buff, sizeof(buff), stdin) != NULL) {
		int length = strlen(buff);
		buff[length-1] = '\0';
		checklocation(fp, buff);
	}

	fclose(fp);
	return 0;
}

